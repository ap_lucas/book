<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Acesso</title>
<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/estilo.css">
</head>
<body>

	<div class="container">
		<div class="page-header">
			<h2 align="center">Acesso</h2>
		</div>
		<div class="row centered">
			<div class="col-sm-4">
				<div class="form-group">
					<label for="usuario">Usu�rio</label>
					<input type="text" class="form-control" id="usuario">
				</div> 
				<div class="form-group">
					<label for="senha">Senha</label>
					<input type="password" class="form-control" id="senha">
				</div>
				<button class="btn btn-default centered">Acessar</button> 
			</div>
		</div><!--  row-->
	</div>

</body>
</html>